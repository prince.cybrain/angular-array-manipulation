import { Component ,OnInit} from '@angular/core';
export class Stack<T> {
  private items: T[];

  constructor() {
    this.items = [];
  }

  push(item: T) {
    this.items.push(item);
  }

  pop(): T | undefined {
    return this.items.pop();
  }

  isEmpty(): boolean {
    return this.items.length === 0;
  }

  peek(): T | undefined {
    return this.items[this.items.length - 1];
  }
}
interface TwoD {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  ContactNo: string;
}
interface Data {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  ContactNo: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'array_manupulation';
//1D
ones: string[] = ['Hello'];

//2D
Two: TwoD[] = [
  {
    Id: 1,
    FirstName: "Prince",
    LastName: "Ray",
    Email: "rayprince@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 2,
    FirstName: "kamal",
    LastName: "k",
    Email: "kamalk@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 3,
    FirstName: "Sri",
    LastName: "KP",
    Email: "kpsri@example.com",
    ContactNo: "555-555-5555"
  }
];

//Push Function
third=this.Two.push({
  Id: 4,
  FirstName: "Alice",
  LastName: "Brown",
  Email: "alicebrown@example.com",
  ContactNo: "555-555-5555"
});

//4.Return type of push function
newLength= this.Two.push()

//5 - No,the push() function cannot be used to add elements to the beginning of an array

//6.To add an element to an array only if it doesn't already exist in the array
newObject={
    Id: 5,
    FirstName: "PK",
    LastName: "Ray",
    Email: "Ray@example.com",
    ContactNo: "555-555-5555"
  };

//7 Spread operator to concatenate two arrays
one: string[] = ['Hello'];
arr2: string[] = ['Sir'];
combine: string[] = [...this.one, ...this.arr2];

//8 Pop() function to remove the last element from an array
pp=this.Two.pop();

//9 The return value of the pop() function is the last element that is popped

//10 Check if an array is empty using the pop() function
arr: string[] = ["1"];
check = this.arr.pop();

//11 No the pop() function be used to remove elements from the beginning of an array

//12 Using the pop() function in combination with the push() function to implement a stack data structure?
myStack: Stack<number> = new Stack<number>();


//13 Using the splice() function to remove elements from an array at a specific index
kk= [
  { id: 1, name: 'Alice', age: 28 },
  { id: 2, name: 'Bob', age: 35 },
  { id: 3, name: 'Charlie', age: 42 }
];
p=this.kk.splice(2, 1);

//14 Using the splice() function to remove a range of elements from an array
pets = [
  { name: "Fluffy", type: "cat", owner: "Alice" },
  { name: "Fido", type: "dog", owner: "Bob" },
  { name: "Ginger", type: "cat", owner: "Charlie" },
  { name: "Rover", type: "dog", owner: "David" }
];
starting=0;
numofelement=2
pt=this.pets.splice(this.starting, this.numofelement); 

//15 Return value of the splice() function
five = ["pk","ks","ma","vk","sj"];
returntype = this.five.splice(2, 2);

//16 Yes the splice() function be used to replace elements in an array.Ex-
num = [1, 2, 3, 4, 5];
replace=this.num.splice(2, 3, 20,40,50);
//17
//slice() is a method that creates a new array containing a copy of a portion of the original array
// splice() is a method that modifies the original array by removing and/or adding elements.

//18 Using the slice() function to copy an entire array
courses = [
  { title: "Math", instructor: "Ms. Smith" },
  { title: "English", instructor: "Mr. Jones" },
  { title: "Science", instructor: "Dr. Patel" }
];
copy = this.courses.slice();

//19
Array = [  { Id: 1, FirstName: "John", LastName: "Doe", Age:23},
  { Id: 2, FirstName: "Jane", LastName: "Doe", Age:27 },
  { Id: 3, FirstName: "Bob", LastName: "Smith", Age:32}];

sort=this.Array.sort((a, b) => b.Id - a.Id);
//20 Return a separate 2D array only having the element IDs of the intial 2D array Hint use map().  
dataSet: Data[] = [
  {
    Id: 1,
    FirstName: "John",
    LastName: "Doe",
    Email: "johndoe@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 2,
    FirstName: "Jane",
    LastName: "Doe",
    Email: "janedoe@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 3,
    FirstName: "Bob",
    LastName: "Smith",
    Email: "bobsmith@example.com",
    ContactNo: "555-555-5555"
  }
];

newArray: number[][] = this.dataSet.map(data => [data.Id]);



constructor(){
  this.myStack.push(1);
    this.myStack.push(2);
    this.myStack.push(3);

    console.log(this.myStack.pop()); 
    console.log(this.myStack.pop()); 
    console.log(this.myStack.peek());
    console.log(this.myStack.isEmpty());
}


ngOnInit(){
  //1
  this.ones = this.ones.concat('Prince','Ray');
  //4
  console.log(this.newLength);
  //6
  if (!this.Two.some(element => element.Id === this.newObject.Id)) {
    this.Two.push(this.newObject);
  }
  //8
  console.log(this.pp);
  //10
  if (this.check === undefined) {
    console.log("Array is empty");
  }
  else{console.log("it is not empty")};
  //12
  

  //13
  console.log(this.kk);
  //14
  console.log(this.pets);
  //15
  console.log(this.returntype);
  console.log(this.five);
  //16
  console.log(this.num);
  //18
  console.log(this.copy); 
  //19
  console.log(this.Array);
  //20
  console.log(this.newArray);
}
}
